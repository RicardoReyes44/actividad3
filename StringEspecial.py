'''
Created on 21 sep. 2020

@author: RSSpe
'''

class StringEspecial:
    "" "Clase para crear un String personalizado y hacerle modificaciones" ""
    def __init__(self):
        self.cadena = ""


    def mostrarCadenaInvertida(self) -> None:
        "" "Metodo para invertir la cadena" ""
        
        if self.cadena=="":
            print("La cadena esta vacia\n")
        elif len(self.cadena)<2:
            print("No notaras la diferencia, prueba cuando tengas mas elementos\n")
        else:
            print(self.cadena[::-1])

            for i in self.cadena.split(" ")[::-1]:
                print(i + " ", end="")
            print("\n")


    def camelCaseEspecial(self) -> None:
        "" "Metodo para convertir mayusculas a minusculas y viceversa" ""
        if self.cadena=="":
            print("La cadena esta vacia\n")
        else:
            print(self.cadena.swapcase()+"\n")


    def primerLetraMayuscula(self) -> None:
        "" "Metodo para invertir la cadena" ""
        if self.cadena=="":
            print("La cadena esta vacia")
        else:
            print(self.cadena.title()+"\n")


    def eliminarCaracter(self) -> None:
        "" "Metodo para eliminar un caracter" ""
        if self.cadena=="":
            print("La cadena esta vacia\n")
        elif len(self.cadena)==1:
            self.cadena=""
            print("Solo habia un elemento asi que ese se elimino")
        else:
            while(True):

                self.posicion = int(input("Introduce posicion: "))
                
                if self.posicion>=0 and self.posicion<len(self.cadena):
                    break;
                else:
                    print("Posicion fuera de rango, por favor prueba de nuevo\n")

            self.cadena = self.cadena[0: self.posicion] + self.cadena[self.posicion+1: len(self.cadena)]


    def eliminarSubCadena(self) -> None:
        "" "Metodo para eliminar una subcadena" ""

        if self.cadena=="":
            print("La cadena esta vacia\n")
        elif len(self.cadena)==1:
            self.cadena = ""
            print("Solo habia un elemento asi que ese se elimino")
        else:
            
            while(True):

                self.rango = input("Introduce rango, ejemplo(3-6): ")
                self.inicio = int(self.rango[0: self.rango.find("-")])
                self.fin = int(self.rango[self.rango.find("-")+1: len(self.rango)])

                if self.inicio>=0 and self.fin<len(self.cadena) and self.inicio<=self.fin and self.rango[self.rango.find("-"): self.rango.find("-")+1]:
                    break;
                else:
                    print("Posicion fuera de rango, por favor prueba de nuevo\n")

            self.cadena = self.cadena[0: self.inicio] + self.cadena[self.fin+1: len(self.cadena)]
        print(self.cadena)


    def agregarCadena(self) -> None:
        "" "Metodo para agregar cadena" ""
        self.candado = True

        while self.candado:

            self.texto = input("Introduce cadena: ")
            
            for i in range(len(self.texto)):
                self.cast = ord(self.texto[i])
                
                if not(self.cast>=97 and self.cast<=122 or self.cast>=65 and self.cast<=90 or self.cast==32 or self.cast==209 or self.cast==241):
                    print("No puedes ingresar caracteres invalidos, ingresa otra cadena\n")
                    break;
                if i==len(self.texto)-1:
                    self.candado = False

        self.cadena = self.texto


    def agregarCadena2(self) -> str:
        "" "Metodo para crear una cadena" ""
        self.candado = True

        while self.candado:

            self.texto = input("Introduce cadena: ")
            
            for i in range(len(self.texto)):
                self.cast = ord(self.texto[i])
                
                if not(self.cast>=97 and self.cast<=122 or self.cast>=65 and self.cast<=90 or self.cast==32 or self.cast==209 or self.cast==241):
                    print("No puedes ingresar caracteres invalidos, ingresa otra cadena\n")
                    break;
                if i==len(self.texto)-1:
                    self.candado = False

        return self.texto


    def agregarElemento(self) -> None:
        "" "Metodo para agregar un elemento o una subcadena" ""
        
        if self.cadena=="":
            self.agregarCadena()
        else:
            while(True):

                self.pos = int(input("Introduce posicion: "))

                if(self.pos>=0 and self.pos<len(self.cadena)):
                    break;
                else:
                    print("posicion fuera de rango, por favor vuelve a intentarlo") 
        
            self.texto = self.agregarCadena2()
            print()
        
            if self.pos==0:
                self.cadena = self.texto + self.cadena
            elif self.pos==len(self.cadena)-1:
                self.cadena = self.cadena + self.texto
            else:
                self.cadena = self.cadena[0: self.pos] + self.texto + self.cadena[self.pos: len(self.cadena)-1]

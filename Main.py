'''
Created on 21 sep. 2020

@author: RSSpe
'''

from StringEspecial import StringEspecial

def main() -> None:
    "" "Funcion principal del programa" ""
    se = StringEspecial()
    candado = True
    
    se.agregarCadena()
    
    while(candado):
        
        try:
            print("\nMENU PRINCIPAL\n1.- Mostrar cadena invertida por letras y palabras")
            print("2.- Agregar caracter o subcadena en posicion especifica")
            print("3.- Eliminar caracter")
            print("4.- Eliminar subcadena")
            print("5.- Mostrar cadena en formato camel case especial")
            print("6.- Mostrar cadena con la primer letra de cada palabra en mayuscula")
            print("7.- Salir")
            opcion = int(input("Introduce opcion: "))
            
            if opcion==1:
                se.mostrarCadenaInvertida()

            elif opcion==2:
                se.agregarElemento()

            elif opcion==3:
                se.eliminarCaracter()

            elif opcion==4:
                se.eliminarSubCadena()

            elif opcion==5:
                se.camelCaseEspecial()

            elif opcion==6:
                se.primerLetraMayuscula()

            elif opcion==7:
                candado = False
            else:
                print("Opcion inexistente, por favor vuelve a intentarlo\n")

        except ValueError as error:
            print("\nEntrada invalida <", error, ">, por favor prueba de nuevo\n")

    print("\n-------------Programa terminado-------------")


if __name__ == '__main__':
    main()
